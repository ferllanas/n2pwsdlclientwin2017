/*
 *  N2PWSDLCltUtils.cpp
 *  N2PWSDLClient
 *
 *  Created by ADMINISTRADOR on 25/04/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "N2PwsdlcltID.h"
#include "N2PWSDLCltUtils.h"

#include "CAlert.h"
//#include "json.h"
//#include "webservices Normal/N2PLocalwsdlBinding.nsmap"
//#include "webservices Normal/soapN2PLocalwsdlBindingProxy.h"
#include <WinSock2.h>
//#include "webservices/soapH.h"
#include "webservices/soapN2PLocalwsdlBindingProxy.h"
#include "webservices/N2PLocalwsdlBinding.nsmap"

#include "WPN2PWSDLCltUtils.h"
#include "WPN2PID.h"

class N2PWSDLCltUtils : public CPMUnknown<IN2PWSDLCltUtils>
{
public:
	/** Constructor.
	 param boss boss object on which this interface is aggregated.
	 */
	N2PWSDLCltUtils (IPMUnknown *boss);
	
	const bool16 realizaConneccion(PMString usuario,
								   PMString ip, 
								   PMString cliente,
								   PMString aplicacion, 
								   PMString URL);
	const bool16 realizaDesconeccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL);
	
	const K2Vector<PMString> MySQLConsulta(PMString query,PMString URL);
	
	const bool16 sendPost(PMString URL, PMString publicacion, PMString idsNotas);
	
	const K2Vector<PMString> MySQLConsulta_function(PMString query,PMString URL, const int32 &counter=0);
	
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
 */
CREATE_PMINTERFACE(N2PWSDLCltUtils, kN2PwsdlcltUtilsImpl)


/* HelloWorld Constructor
 */
N2PWSDLCltUtils::N2PWSDLCltUtils(IPMUnknown* boss) 
: CPMUnknown<IN2PWSDLCltUtils>(boss)
{
	// do nothing.
	
	/* NOTE: 
	 There used to be code that would set the preference value by default.
	 That was removed in favor of the workspace responders.
	 
	 The responders get called, and they will try to setup the value
	 for this custom preference.  It is one of the responders that will
	 set the correct initial value for the cached preference, fPrefState.
	 
	 If the global workspace is already created from a previous 
	 application session, the ReadWrite() method will set the 
	 cached preference, fPrefState. The same ReadWrite() will be called when 
	 an existing document is opened.
	 
	 Refer to CstPrfNewWSResponder.cpp and N2PSQLUtils.cpp
	 for details on how to set the workspace defaults. 
	 */
}



const bool16 N2PWSDLCltUtils::realizaConneccion(PMString usuario, PMString ip, PMString cliente, PMString aplicacion, PMString URL)
{
	bool16 retval = kFalse;
	ns1__STConnection STConnectionZZ;

	ns1__ValidaConnectionResponse param_1;
	STConnectionZZ.username = (char *)usuario.GrabTString();//const_cast<char *>(usuario.GetPlatformString().c_str()); //"Disenador"; //new char[usuario.GetUTF8String().size() + 1];
	//std::copy(usuario.GetUTF8String().begin(), usuario.GetUTF8String().end(), STConnectionZZ.username);//usuario.GetUTF8String();// .c_str();
	
	STConnectionZZ.ip = (char *)ip.GrabTString();//"192.168.213.40";//(char *) ip.GetUTF8String().c_str();// new char[ip.GetUTF8String().size() + 1];
	//std::copy(ip.GetUTF8String().begin(), ip.GetUTF8String().end(), STConnectionZZ.ip);

	STConnectionZZ.cliente = (char *)cliente.GrabTString();//"3";//(char *)cliente.GetUTF8String().c_str();// new char[cliente.GetUTF8String().size() + 1];
	//std::copy(cliente.GetUTF8String().begin(), cliente.GetUTF8String().end(), STConnectionZZ.cliente);
	STConnectionZZ.aplicacion = (char *)aplicacion.GrabTString();//"2";// (char *)aplicacion.GetUTF8String().c_str();// new char[aplicacion.GetUTF8String().size() + 1];
	//std::copy(aplicacion.GetUTF8String().begin(), aplicacion.GetUTF8String().end(), STConnectionZZ.aplicacion);
	//aplicacion.GetUTF8String().c_str();
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.connect_timeout = 10;
	N2PLocal.linger_time = 10;
	N2PLocal.recv_timeout = 10;
	N2PLocal.send_timeout = 10;
	N2PLocal.accept_timeout = 10;
	//N2PLocal.transfer_timeout = 10;
	N2PLocal.soap_endpoint = URL.GetUTF8String().c_str();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.ValidaConnection(&STConnectionZZ,param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		
		PMString ErrorS=N2PLocal.soap_fault_string();
		if(ErrorS.Contains("404 Not Found"))
		{
			CAlert::InformationAlert(kN2PwsdlcltNoConnectWebServicesStringKey);
		}
		else {
			PMString myerror = URL;
			myerror.Append(". Error: ");
			myerror.Append(N2PLocal.soap_fault_string());
			myerror.Append(". Detalle: ");
			myerror.Append(N2PLocal.soap_fault_detail());
			CAlert::InformationAlert(myerror);
		}
		
		
		
		
	}
	else
	{
		PMString  XCS="";
		XCS.Append(param_1.return_->errorstring);// .c_str());//;
		if(XCS=="OK")
		{
			retval=kTrue;
		}
		else
		{
			//XCS.Append(" realizaConneccion");
			CAlert::InformationAlert(XCS);
		}
	}
	
	return retval;
}

const bool16 N2PWSDLCltUtils::realizaDesconeccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL)
{
	
	bool16 retval=kFalse;
	ns1__STConnection STConnectionZZ;
	ns1__disconnectResponse param_1;
	
	STConnectionZZ.username = (char *)usuario.GrabTString();//new char[usuario.GetUTF8String().size() + 1];
	//usuario.GetUTF8String().c_str();
	STConnectionZZ.ip = (char *)ip.GrabTString();//new char[ip.GetUTF8String().size() + 1];
	//ip.GetUTF8String().c_str();
	STConnectionZZ.cliente = (char *)cliente.GrabTString();//new char[cliente.GetUTF8String().size() + 1];
	//= cliente.GetUTF8String().c_str();
	STConnectionZZ.aplicacion = (char *)aplicacion.GrabTString();// new char[aplicacion.GetUTF8String().size() + 1];
	//std::copy(aplicacion.GetUTF8String().begin(), aplicacion.GetUTF8String().end(), STConnectionZZ.aplicacion);
	//aplicacion.GetUTF8String().c_str();
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.connect_timeout = 10;
	N2PLocal.linger_time = 10;
	N2PLocal.recv_timeout = 10;
	N2PLocal.send_timeout = 10;
	N2PLocal.accept_timeout = 10;
	//N2PLocal.transfer_timeout = 10;
	N2PLocal.soap_endpoint = URL.GetUTF8String().c_str();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.disconnect(&STConnectionZZ, param_1);
	//.ValidaConnection(&STConnectionZZ,param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		
		PMString ErrorS=N2PLocal.soap_fault_string();
		if(ErrorS.Contains("404 Not Found"))
		{
			CAlert::InformationAlert(kN2PwsdlcltNoConnectWebServicesStringKey);
		}
		else {
			PMString myerror = URL;
			myerror.Append(". Error: ");
			myerror.Append(N2PLocal.soap_fault_string());
			myerror.Append(". Detalle: ");
			myerror.Append(N2PLocal.soap_fault_detail());
			CAlert::InformationAlert(myerror);
		}
		
		
		
		
	}
	else
	{
		PMString  XCS="";
		XCS.Append(param_1.return_->errorstring);// .c_str());//;
		
		if(XCS=="OK")
		{
			retval=kTrue;
			
		}
		else
		{
			//XCS.Append(" realizaConneccion");
			//CAlert::InformationAlert(XCS+" Que rollo 03");
			ASSERT_FAIL("No pudo hacer desconexion");
		}
	}
	return retval;
}



const K2Vector<PMString> N2PWSDLCltUtils::MySQLConsulta(PMString query,PMString URL)
{
	K2Vector<PMString> retval;
    
	std::string consulta= query.GetUTF8String().c_str();
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.connect_timeout = 10;
	N2PLocal.linger_time = 10;
	N2PLocal.recv_timeout = 10;
	N2PLocal.send_timeout = 10;
	N2PLocal.accept_timeout = 10;
	//N2PLocal.transfer_timeout = 10;
	N2PLocal.soap_endpoint = URL.GetUTF8String().c_str();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	
	ns1__EjecutaQueryResponse param_1;
	
	N2PLocal.soap_endpoint = URL.GetUTF8String().c_str();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.EjecutaQuery( (char *)consulta.c_str() , param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		
		PMString ErrorS=N2PLocal.soap_fault_string();
		if(ErrorS.Contains("404 Not Found"))
		{
			CAlert::InformationAlert(kN2PwsdlcltNoConnectWebServicesStringKey);
		}
		else {
			PMString myerror = URL;
			myerror.Append(". Error: ");
			myerror.Append(N2PLocal.soap_fault_string());
			myerror.Append(". Detalle: ");
			myerror.Append(N2PLocal.soap_fault_detail());
			CAlert::InformationAlert(myerror);
		}
		
		
		
		
	}
	else
	{
		ns1__campo AS;
		
		for(int32 i=0; i < param_1.return_->Registros->__size; i++)
		{
			for(int32 j=0;j<param_1.return_->Registros[i].__size; j++)
			{
				PMString cantidad2 = param_1.return_->Registros[i].__ptr[j]->row;// .c_str();
				CAlert::InformationAlert(cantidad2);
			}
		}
	}	
	return retval;
}



const bool16 N2PWSDLCltUtils::sendPost(PMString URL, PMString publicacion, PMString idsNotas)
{
	bool16 retval=kFalse;
	
	do{
		
		InterfacePtr<IWPN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IWPN2PWSDLCltUtils*> (CreateObject
																						  (
																						   kWPN2PwsdlcltUtilsBoss,	// Object boss/class
																						   IWPN2PWSDLCltUtils::kDefaultIID
																						   )));
		
		if(!	N2PWSDLCltUtil)
		{
			CAlert::InformationAlert("salio");
			break;
		}
		PMString ResultString="OK";
		
		retval=N2PWSDLCltUtil->sendPost(URL, publicacion, idsNotas );//realizaConneccion("ferllanas",
		
		
	}while(false);
	
	return retval;
}



const K2Vector<PMString> N2PWSDLCltUtils::MySQLConsulta_function(PMString query,PMString URL,const int32 &counter)
{
   
	K2Vector<PMString> retval;
    std::string consulta= query.GetUTF8String().c_str();//.GetPlatformString(PMString::kEncodingASCII);//
    
    N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	//N2PLocal.socket_flags = MSG_NOSIGNAL;
	//N2PLocal.accept_flags = SO_NOSIGPIPE;
    N2PLocal.connect_timeout= 10;
    N2PLocal.linger_time= 10;
    N2PLocal.recv_timeout= 10;
    N2PLocal.send_timeout= 10;
    N2PLocal.accept_timeout= 10;
	//N2PLocal.transfer_timeout = 10;
	///N2PLocal.socket_flags = 
	N2PLocal.soap_endpoint = URL.GetUTF8String().c_str();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	
	ns1__MYSQLQuery_USCOREfunctionResponse param_1;

    N2PLocal.soap_endpoint = URL.GetUTF8String().c_str();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
    
	//CAlert::InformationAlert("ini Consulta");
    N2PLocal.MYSQLQuery_USCOREfunction( (char *)consulta.c_str() , param_1);
	//CAlert::InformationAlert("Finaliza Consulta");
    
    if(N2PLocal.error)//soap->error)
	{
       // CAlert::InformationAlert("Error "+query+" "+URL);
		N2PLocal.soap_stream_fault(std::cerr);
		//CAlert::InformationAlert("Error 01");
		PMString SAL="MySQLConsulta_function";
		SAL.Append("/n ");
		SAL.Append(query);		
		SAL.Append("/n URL:");
        SAL.Append(URL);
        SAL.Append("/n ");
        
       // CAlert::InformationAlert("Error 02");
		SAL.Append(N2PLocal.soap_fault_string());
		//CAlert::InformationAlert("Error 03");
        PMString errorWebServices = N2PLocal.soap_fault_string();
        // PMString detailerrorWebServices = N2PLocal.soap_fault_detail();
       // CAlert::InformationAlert("Error 04 "+errorWebServices);//+" ,detail:"+detailerrorWebServices);
        if(counter==10){
            CAlert::InformationAlert("Error "+URL+", "+query+", "+errorWebServices);
            return retval;
        }
        else
        {

            CAlert::InformationAlert("Error " + URL + ", " + query + ", " + errorWebServices);
            if(errorWebServices.Contains("404"))
            {
                CAlert::InformationAlert(kN2PwsdlcltNoConnectWebServicesStringKey);
                return retval;
            }
        // CAlert::InformationAlert("Error 06");
            if(errorWebServices.Contains("400"))
            {
				//N2PLocal.disconnect();
				N2PLocal.destroy();
                return MySQLConsulta_function(query,URL, counter+1);
            }
        // CAlert::InformationAlert("Error 07");
            if(errorWebServices.Contains("200"))
            {
				//N2PLocal.disconnect();
				N2PLocal.destroy();
                return MySQLConsulta_function(query,URL, counter+1);
            }
       //  CAlert::InformationAlert("Error 08");
            if(errorWebServices.Contains("Host not found"))
            {
				//N2PLocal.disconnect();
				N2PLocal.destroy();
                return MySQLConsulta_function(query,URL, counter+1);
            }
       //  CAlert::InformationAlert("Error 09");
            if(errorWebServices.Contains("No routed to host"))
            {
				//N2PLocal.disconnect();
				N2PLocal.destroy();
				//N2PLocal.disconnect()
                return MySQLConsulta_function(query,URL, counter+1);
            }

			if (errorWebServices.Contains("No data"))
			{
				//N2PLocal.disconnect();
				N2PLocal.destroy();
				//N2PLocal.disconnect()
				return MySQLConsulta_function(query, URL, counter + 1);
			}
        }
	}
	else
	{
		ns1__campo AS;
        //CAlert::InformationAlert("Sin Error");
		for(int32 i=0; i < 1; i++)
		{
            //CAlert::InformationAlert("Sin Error 01");
			for(int32 j=0;j<param_1.return_->registroArray[i].__size; j++)
			{
               // CAlert::InformationAlert("Sin Error 02");
				PMString registro="";
				for(int32 k=0;k<param_1.return_->registroArray[i].__ptr[j]->__size; k++)
				{
                    //CAlert::InformationAlert("Sin Error 03");
					PMString campo = param_1.return_->registroArray[i].__ptr[j]->__ptr[k]->nombre;// .c_str();
					PMString contenido = param_1.return_->registroArray[i].__ptr[j]->__ptr[k]->contenido;// .c_str();
					registro.Append(campo);
					registro.Append(":");
					registro.Append(contenido);
					registro.Append("©");
				}
                //CAlert::InformationAlert("Sin Error 04");
				retval.push_back(registro);
            }
		}
	}
	//N2PLocal.disconnect();
	N2PLocal.destroy();
	return retval;
}

