/* soapN2PLocalwsdlBindingProxy.cpp
   Generated by gSOAP 2.8.54 for N2PLocalWsdl.h

gSOAP XML Web services tools
Copyright (C) 2000-2017, Robert van Engelen, Genivia Inc. All Rights Reserved.
The soapcpp2 tool and its generated software are released under the GPL.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
--------------------------------------------------------------------------------
A commercial use license is available from Genivia Inc., contact@genivia.com
--------------------------------------------------------------------------------
*/
#include "VCPlugInHeaders.h"
#include "soapN2PLocalwsdlBindingProxy.h"

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy()
{	this->soap = soap_new();
	this->soap_own = true;
	N2PLocalwsdlBindingProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy(const N2PLocalwsdlBindingProxy& rhs)
{	this->soap = rhs.soap;
	this->soap_own = false;
	this->soap_endpoint = rhs.soap_endpoint;
}

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy(struct soap *_soap)
{	this->soap = _soap;
	this->soap_own = false;
	N2PLocalwsdlBindingProxy_init(_soap->imode, _soap->omode);
}

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy(const char *endpoint)
{	this->soap = soap_new();
	this->soap_own = true;
	N2PLocalwsdlBindingProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
	soap_endpoint = endpoint;
}

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy(soap_mode iomode)
{	this->soap = soap_new();
	this->soap_own = true;
	N2PLocalwsdlBindingProxy_init(iomode, iomode);
}

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy(const char *endpoint, soap_mode iomode)
{	this->soap = soap_new();
	this->soap_own = true;
	N2PLocalwsdlBindingProxy_init(iomode, iomode);
	soap_endpoint = endpoint;
}

N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy(soap_mode imode, soap_mode omode)
{	this->soap = soap_new();
	this->soap_own = true;
	N2PLocalwsdlBindingProxy_init(imode, omode);
}

N2PLocalwsdlBindingProxy::~N2PLocalwsdlBindingProxy()
{	if (this->soap_own)
		soap_free(this->soap);
}

void N2PLocalwsdlBindingProxy::N2PLocalwsdlBindingProxy_init(soap_mode imode, soap_mode omode)
{	soap_imode(this->soap, imode);
	soap_omode(this->soap, omode);
	soap_endpoint = NULL;
	static const struct Namespace namespaces[] = {
        {"SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/", "http://www.w3.org/*/soap-envelope", NULL},
        {"SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/", "http://www.w3.org/*/soap-encoding", NULL},
        {"xsi", "http://www.w3.org/2001/XMLSchema-instance", "http://www.w3.org/*/XMLSchema-instance", NULL},
        {"xsd", "http://www.w3.org/2001/XMLSchema", "http://www.w3.org/*/XMLSchema", NULL},
        {"ns1", "urn:N2PLocalwsdl", NULL, NULL},
        {NULL, NULL, NULL, NULL}
    };
	soap_set_namespaces(this->soap, namespaces);
}

N2PLocalwsdlBindingProxy *N2PLocalwsdlBindingProxy::copy()
{	N2PLocalwsdlBindingProxy *dup = SOAP_NEW_COPY(N2PLocalwsdlBindingProxy);
	if (dup)
		soap_copy_context(dup->soap, this->soap);
	return dup;
}

N2PLocalwsdlBindingProxy& N2PLocalwsdlBindingProxy::operator=(const N2PLocalwsdlBindingProxy& rhs)
{	if (this->soap != rhs.soap)
	{	if (this->soap_own)
			soap_free(this->soap);
		this->soap = rhs.soap;
		this->soap_own = false;
		this->soap_endpoint = rhs.soap_endpoint;
	}
	return *this;
}

void N2PLocalwsdlBindingProxy::destroy()
{	soap_destroy(this->soap);
	soap_end(this->soap);
}

void N2PLocalwsdlBindingProxy::reset()
{	this->destroy();
	soap_done(this->soap);
	soap_initialize(this->soap);
	N2PLocalwsdlBindingProxy_init(SOAP_IO_DEFAULT, SOAP_IO_DEFAULT);
}

void N2PLocalwsdlBindingProxy::soap_noheader()
{	this->soap->header = NULL;
}

::SOAP_ENV__Header *N2PLocalwsdlBindingProxy::soap_header()
{	return this->soap->header;
}

::SOAP_ENV__Fault *N2PLocalwsdlBindingProxy::soap_fault()
{	return this->soap->fault;
}

const char *N2PLocalwsdlBindingProxy::soap_fault_string()
{	return *soap_faultstring(this->soap);
}

const char *N2PLocalwsdlBindingProxy::soap_fault_detail()
{	return *soap_faultdetail(this->soap);
}

int N2PLocalwsdlBindingProxy::soap_close_socket()
{	return soap_closesock(this->soap);
}

int N2PLocalwsdlBindingProxy::soap_force_close_socket()
{	return soap_force_closesock(this->soap);
}

void N2PLocalwsdlBindingProxy::soap_print_fault(FILE *fd)
{	::soap_print_fault(this->soap, fd);
}

#ifndef WITH_LEAN
#ifndef WITH_COMPAT
void N2PLocalwsdlBindingProxy::soap_stream_fault(std::ostream& os)
{	::soap_stream_fault(this->soap, os);
}
#endif

char *N2PLocalwsdlBindingProxy::soap_sprint_fault(char *buf, size_t len)
{	return ::soap_sprint_fault(this->soap, buf, len);
}
#endif

int N2PLocalwsdlBindingProxy::getStuffs(const char *endpoint, const char *soap_action, const std::string& user, const std::string& pass, struct ns1__getStuffsResponse &_param_1)
{	struct soap *soap = this->soap;
	struct ns1__getStuffs soap_tmp_ns1__getStuffs;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://192.168.213.158/Coppel/news2page_licences_ws_php/N2PServerwsdl.php";
	if (soap_action == NULL)
		soap_action = "urn:N2PLocalwsdl#getStuffs";
	soap_tmp_ns1__getStuffs.user = user;
	soap_tmp_ns1__getStuffs.pass = pass;
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_serializeheader(soap);
	soap_serialize_ns1__getStuffs(soap, &soap_tmp_ns1__getStuffs);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__getStuffs(soap, &soap_tmp_ns1__getStuffs, "ns1:getStuffs", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__getStuffs(soap, &soap_tmp_ns1__getStuffs, "ns1:getStuffs", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!static_cast<struct ns1__getStuffsResponse*>(&_param_1)) // NULL ref?
		return soap_closesock(soap);
	soap_default_ns1__getStuffsResponse(soap, &_param_1);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_get_ns1__getStuffsResponse(soap, &_param_1, "", NULL);
	if (soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}

int N2PLocalwsdlBindingProxy::ValidaConnection(const char *endpoint, const char *soap_action, ns1__STConnection *STConnectionZZ, struct ns1__ValidaConnectionResponse &_param_1)
{	struct soap *soap = this->soap;
	struct ns1__ValidaConnection soap_tmp_ns1__ValidaConnection;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://192.168.213.158/Coppel/news2page_licences_ws_php/N2PServerwsdl.php";
	if (soap_action == NULL)
		soap_action = "urn:N2PLocalwsdl#ValidaConnection";
	soap_tmp_ns1__ValidaConnection.STConnectionZZ = STConnectionZZ;
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_serializeheader(soap);
	soap_serialize_ns1__ValidaConnection(soap, &soap_tmp_ns1__ValidaConnection);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__ValidaConnection(soap, &soap_tmp_ns1__ValidaConnection, "ns1:ValidaConnection", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__ValidaConnection(soap, &soap_tmp_ns1__ValidaConnection, "ns1:ValidaConnection", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!static_cast<struct ns1__ValidaConnectionResponse*>(&_param_1)) // NULL ref?
		return soap_closesock(soap);
	soap_default_ns1__ValidaConnectionResponse(soap, &_param_1);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_get_ns1__ValidaConnectionResponse(soap, &_param_1, "", NULL);
	if (soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}

int N2PLocalwsdlBindingProxy::disconnect(const char *endpoint, const char *soap_action, ns1__STConnection *STConnectionZZ, struct ns1__disconnectResponse &_param_1)
{	struct soap *soap = this->soap;
	struct ns1__disconnect soap_tmp_ns1__disconnect;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://192.168.213.158/Coppel/news2page_licences_ws_php/N2PServerwsdl.php";
	if (soap_action == NULL)
		soap_action = "urn:N2PLocalwsdl#ValidaConnection";
	soap_tmp_ns1__disconnect.STConnectionZZ = STConnectionZZ;
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_serializeheader(soap);
	soap_serialize_ns1__disconnect(soap, &soap_tmp_ns1__disconnect);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__disconnect(soap, &soap_tmp_ns1__disconnect, "ns1:disconnect", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__disconnect(soap, &soap_tmp_ns1__disconnect, "ns1:disconnect", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!static_cast<struct ns1__disconnectResponse*>(&_param_1)) // NULL ref?
		return soap_closesock(soap);
	soap_default_ns1__disconnectResponse(soap, &_param_1);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_get_ns1__disconnectResponse(soap, &_param_1, "", NULL);
	if (soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}

int N2PLocalwsdlBindingProxy::EjecutaQuery(const char *endpoint, const char *soap_action, const std::string& consulta, struct ns1__EjecutaQueryResponse &_param_1)
{	struct soap *soap = this->soap;
	struct ns1__EjecutaQuery soap_tmp_ns1__EjecutaQuery;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://192.168.213.158/Coppel/news2page_licences_ws_php/N2PServerwsdl.php";
	if (soap_action == NULL)
		soap_action = "urn:N2PLocalwsdl#EjecutaQuery";
	soap_tmp_ns1__EjecutaQuery.consulta = consulta;
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_serializeheader(soap);
	soap_serialize_ns1__EjecutaQuery(soap, &soap_tmp_ns1__EjecutaQuery);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__EjecutaQuery(soap, &soap_tmp_ns1__EjecutaQuery, "ns1:EjecutaQuery", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__EjecutaQuery(soap, &soap_tmp_ns1__EjecutaQuery, "ns1:EjecutaQuery", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!static_cast<struct ns1__EjecutaQueryResponse*>(&_param_1)) // NULL ref?
		return soap_closesock(soap);
	soap_default_ns1__EjecutaQueryResponse(soap, &_param_1);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_get_ns1__EjecutaQueryResponse(soap, &_param_1, "", NULL);
	if (soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}

int N2PLocalwsdlBindingProxy::MYSQLQuery_USCOREfunction(const char *endpoint, const char *soap_action, const std::string& consulta, struct ns1__MYSQLQuery_USCOREfunctionResponse &_param_1)
{	struct soap *soap = this->soap;
	struct ns1__MYSQLQuery_USCOREfunction soap_tmp_ns1__MYSQLQuery_USCOREfunction;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://192.168.213.158/Coppel/news2page_licences_ws_php/N2PServerwsdl.php";
	if (soap_action == NULL)
		soap_action = "urn:N2PLocalwsdl#MYSQLQuery_function";
	soap_tmp_ns1__MYSQLQuery_USCOREfunction.consulta = consulta;
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_serializeheader(soap);
	soap_serialize_ns1__MYSQLQuery_USCOREfunction(soap, &soap_tmp_ns1__MYSQLQuery_USCOREfunction);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__MYSQLQuery_USCOREfunction(soap, &soap_tmp_ns1__MYSQLQuery_USCOREfunction, "ns1:MYSQLQuery_function", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__MYSQLQuery_USCOREfunction(soap, &soap_tmp_ns1__MYSQLQuery_USCOREfunction, "ns1:MYSQLQuery_function", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!static_cast<struct ns1__MYSQLQuery_USCOREfunctionResponse*>(&_param_1)) // NULL ref?
		return soap_closesock(soap);
	soap_default_ns1__MYSQLQuery_USCOREfunctionResponse(soap, &_param_1);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_get_ns1__MYSQLQuery_USCOREfunctionResponse(soap, &_param_1, "", NULL);
	if (soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}

int N2PLocalwsdlBindingProxy::WFC_USCOREMYSQLQuery_USCOREfunction(const char *endpoint, const char *soap_action, const std::string& consulta, struct ns1__WFC_USCOREMYSQLQuery_USCOREfunctionResponse &_param_1)
{	struct soap *soap = this->soap;
	struct ns1__WFC_USCOREMYSQLQuery_USCOREfunction soap_tmp_ns1__WFC_USCOREMYSQLQuery_USCOREfunction;
	if (endpoint)
		soap_endpoint = endpoint;
	if (soap_endpoint == NULL)
		soap_endpoint = "http://192.168.213.158/Coppel/news2page_licences_ws_php/N2PServerwsdl.php";
	if (soap_action == NULL)
		soap_action = "urn:N2PLocalwsdl#WFC_MYSQLQuery_function";
	soap_tmp_ns1__WFC_USCOREMYSQLQuery_USCOREfunction.consulta = consulta;
	soap_begin(soap);
	soap->encodingStyle = "http://schemas.xmlsoap.org/soap/encoding/";
	soap_serializeheader(soap);
	soap_serialize_ns1__WFC_USCOREMYSQLQuery_USCOREfunction(soap, &soap_tmp_ns1__WFC_USCOREMYSQLQuery_USCOREfunction);
	if (soap_begin_count(soap))
		return soap->error;
	if (soap->mode & SOAP_IO_LENGTH)
	{	if (soap_envelope_begin_out(soap)
		 || soap_putheader(soap)
		 || soap_body_begin_out(soap)
		 || soap_put_ns1__WFC_USCOREMYSQLQuery_USCOREfunction(soap, &soap_tmp_ns1__WFC_USCOREMYSQLQuery_USCOREfunction, "ns1:WFC_MYSQLQuery_function", "")
		 || soap_body_end_out(soap)
		 || soap_envelope_end_out(soap))
			 return soap->error;
	}
	if (soap_end_count(soap))
		return soap->error;
	if (soap_connect(soap, soap_endpoint, soap_action)
	 || soap_envelope_begin_out(soap)
	 || soap_putheader(soap)
	 || soap_body_begin_out(soap)
	 || soap_put_ns1__WFC_USCOREMYSQLQuery_USCOREfunction(soap, &soap_tmp_ns1__WFC_USCOREMYSQLQuery_USCOREfunction, "ns1:WFC_MYSQLQuery_function", "")
	 || soap_body_end_out(soap)
	 || soap_envelope_end_out(soap)
	 || soap_end_send(soap))
		return soap_closesock(soap);
	if (!static_cast<struct ns1__WFC_USCOREMYSQLQuery_USCOREfunctionResponse*>(&_param_1)) // NULL ref?
		return soap_closesock(soap);
	soap_default_ns1__WFC_USCOREMYSQLQuery_USCOREfunctionResponse(soap, &_param_1);
	if (soap_begin_recv(soap)
	 || soap_envelope_begin_in(soap)
	 || soap_recv_header(soap)
	 || soap_body_begin_in(soap))
		return soap_closesock(soap);
	if (soap_recv_fault(soap, 1))
		return soap->error;
	soap_get_ns1__WFC_USCOREMYSQLQuery_USCOREfunctionResponse(soap, &_param_1, "", NULL);
	if (soap->error)
		return soap_recv_fault(soap, 0);
	if (soap_body_end_in(soap)
	 || soap_envelope_end_in(soap)
	 || soap_end_recv(soap))
		return soap_closesock(soap);
	return soap_closesock(soap);
}
/* End of client proxy code */
