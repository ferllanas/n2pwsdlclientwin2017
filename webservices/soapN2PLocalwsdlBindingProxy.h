/* soapN2PLocalwsdlBindingProxy.h
   Generated by gSOAP 2.8.14 from N2PLocalwsdl.h

Copyright(C) 2000-2013, Robert van Engelen, Genivia Inc. All Rights Reserved.
The generated code is released under ONE of the following licenses:
GPL or Genivia's license for commercial use.
This program is released under the GPL with the additional exemption that
compiling, linking, and/or using OpenSSL is allowed.
*/

#ifndef soapN2PLocalwsdlBindingProxy_H
#define soapN2PLocalwsdlBindingProxy_H
#include "soapH.h"

class SOAP_CMAC N2PLocalwsdlBindingProxy : public soap
{ public:
	/// Endpoint URL of service 'N2PLocalwsdlBindingProxy' (change as needed)
	const char *soap_endpoint;
	/// Constructor
	N2PLocalwsdlBindingProxy();
	/// Construct from another engine state
	N2PLocalwsdlBindingProxy(const struct soap&);
	/// Constructor with endpoint URL
	N2PLocalwsdlBindingProxy(const char *url);
	/// Constructor with engine input+output mode control
	N2PLocalwsdlBindingProxy(soap_mode iomode);
	/// Constructor with URL and input+output mode control
	N2PLocalwsdlBindingProxy(const char *url, soap_mode iomode);
	/// Constructor with engine input and output mode control
	N2PLocalwsdlBindingProxy(soap_mode imode, soap_mode omode);
	/// Destructor frees deserialized data
	virtual	~N2PLocalwsdlBindingProxy();
	/// Initializer used by constructors
	virtual	void N2PLocalwsdlBindingProxy_init(soap_mode imode, soap_mode omode);
	/// Delete all deserialized data (uses soap_destroy and soap_end)
	virtual	void destroy();
	/// Delete all deserialized data and reset to default
	virtual	void reset();
	/// Disables and removes SOAP Header from message
	virtual	void soap_noheader();
	/// Get SOAP Header structure (NULL when absent)
	virtual	const SOAP_ENV__Header *soap_header();
	/// Get SOAP Fault structure (NULL when absent)
	virtual	const SOAP_ENV__Fault *soap_fault();
	/// Get SOAP Fault string (NULL when absent)
	virtual	const char *soap_fault_string();
	/// Get SOAP Fault detail as string (NULL when absent)
	virtual	const char *soap_fault_detail();
	/// Close connection (normally automatic, except for send_X ops)
	virtual	int soap_close_socket();
	/// Force close connection (can kill a thread blocked on IO)
	virtual	int soap_force_close_socket();
	/// Print fault
	virtual	void soap_print_fault(FILE*);
#ifndef WITH_LEAN
	/// Print fault to stream
#ifndef WITH_COMPAT
	virtual	void soap_stream_fault(std::ostream&);
#endif

	/// Put fault into buffer
	virtual	char *soap_sprint_fault(char *buf, size_t len);
#endif

	/// Web service operation 'getStuffs' (returns error code or SOAP_OK)
	virtual	int getStuffs(char *user, char *pass, struct ns1__getStuffsResponse &_param_1) { return this->getStuffs(NULL, NULL, user, pass, _param_1); }
	virtual	int getStuffs(const char *endpoint, const char *soap_action, char *user, char *pass, struct ns1__getStuffsResponse &_param_1);

	/// Web service operation 'ValidaConnection' (returns error code or SOAP_OK)
	virtual	int ValidaConnection(ns1__STConnection *STConnectionZZ, struct ns1__ValidaConnectionResponse &_param_2) { return this->ValidaConnection(NULL, NULL, STConnectionZZ, _param_2); }
	virtual	int ValidaConnection(const char *endpoint, const char *soap_action, ns1__STConnection *STConnectionZZ, struct ns1__ValidaConnectionResponse &_param_2);

	/// Web service operation 'disconnect' (returns error code or SOAP_OK)
	virtual	int disconnect(ns1__STConnection *STConnectionZZ, struct ns1__disconnectResponse &_param_3) { return this->disconnect(NULL, NULL, STConnectionZZ, _param_3); }
	virtual	int disconnect(const char *endpoint, const char *soap_action, ns1__STConnection *STConnectionZZ, struct ns1__disconnectResponse &_param_3);

	/// Web service operation 'EjecutaQuery' (returns error code or SOAP_OK)
	virtual	int EjecutaQuery(char *consulta, struct ns1__EjecutaQueryResponse &_param_4) { return this->EjecutaQuery(NULL, NULL, consulta, _param_4); }
	virtual	int EjecutaQuery(const char *endpoint, const char *soap_action, char *consulta, struct ns1__EjecutaQueryResponse &_param_4);

	/// Web service operation 'MYSQLQuery_function' (returns error code or SOAP_OK)
	virtual	int MYSQLQuery_USCOREfunction(char *consulta, struct ns1__MYSQLQuery_USCOREfunctionResponse &_param_5) { return this->MYSQLQuery_USCOREfunction(NULL, NULL, consulta, _param_5); }
	virtual	int MYSQLQuery_USCOREfunction(const char *endpoint, const char *soap_action, char *consulta, struct ns1__MYSQLQuery_USCOREfunctionResponse &_param_5);

	/// Web service operation 'WFC_MYSQLQuery_function' (returns error code or SOAP_OK)
	virtual	int WFC_USCOREMYSQLQuery_USCOREfunction(char *consulta, struct ns1__WFC_USCOREMYSQLQuery_USCOREfunctionResponse &_param_6) { return this->WFC_USCOREMYSQLQuery_USCOREfunction(NULL, NULL, consulta, _param_6); }
	virtual	int WFC_USCOREMYSQLQuery_USCOREfunction(const char *endpoint, const char *soap_action, char *consulta, struct ns1__WFC_USCOREMYSQLQuery_USCOREfunctionResponse &_param_6);
};
#endif
