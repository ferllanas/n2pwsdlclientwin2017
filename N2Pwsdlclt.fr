//========================================================================================
//  
//  $File: $
//  
//  Owner: INTERLASA.COM S.A. DE C.V.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"
#include "PanelList.fh"
#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss

#include "EveInfo.fh"	// Required when using EVE for dialog layout/widget placement

// Project includes:
#include "N2PwsdlcltID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kN2PwsdlcltPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kN2PwsdlcltCurrentMajorFormatNumber, kN2PwsdlcltCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct },
	{ kWildFS },
	kUIPlugIn,
	kN2PwsdlcltVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kN2PwsdlcltCompanyValue,			// Company name
	kN2PwsdlcltMissingPluginURLValue,	// URL 
	kN2PwsdlcltMissingPluginAlertValue,	// Missing plug-in alert text
};

/* Boss class definition.
*/
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

    /**
    This boss class supports two interfaces:
    IActionComponent and IPMPersist.


    @ingroup n2pwsdlclient
    */
    Class
    {
        kN2PwsdlcltActionComponentBoss,
        kInvalidClass,
        {
            /** Handle the actions from the menu. */
            IID_IACTIONCOMPONENT, kN2PwsdlcltActionComponentImpl,
            /** Persist the state of the menu across application instantiation.
            Implementation provided by the API.*/
            IID_IPMPERSIST, kPMPersistImpl
        }
    },

    Class
    {
        kN2PwsdlcltUtilsBoss,
        kInvalidClass,
        {
            IID_IN2PWSDLCLTUTILSINTERFACE, kN2PwsdlcltUtilsImpl,
        }
    }

}}};

/*  Implementation definition.
*/
resource FactoryList (kSDKDefFactoryListResourceID)
{
    kImplementationIDSpace,
    {
        #include "N2PwsdlcltFactoryList.h"
    }
};


/*  Menu definition.
*/
resource MenuDef (kSDKDefMenuResourceID)
{
    {
        // The About Plug-ins sub-menu item for this plug-in.
        kN2PwsdlcltAboutActionID,			// ActionID (kInvalidActionID for positional entries)
        kN2PwsdlcltAboutMenuPath,			// Menu Path.
        kSDKDefAlphabeticPosition,			// Menu Position.
        kSDKDefIsNotDynamicMenuFlag,		// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag


    }
};

/* Action definition.
*/
resource ActionDef (kSDKDefActionResourceID)
{
    {
        kN2PwsdlcltActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
        kN2PwsdlcltAboutActionID,	// ActionID.
        kN2PwsdlcltAboutMenuKey,	// Sub-menu string.
        kOtherActionArea,				// Area name (see ActionDefs.h).
        kNormalAction,					// Type of action (see ActionDefs.h).
        kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
        kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
        kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.



    }
};


/*  LocaleIndex
The LocaleIndex should have indicies that point at your
localizations for each language system that you are localized for.
*/
/*  String LocaleIndex.
*/
resource LocaleIndex ( kSDKDefStringsResourceID)
{
    kStringTableRsrcType,
    {
        kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_esES, kSDKDefStringsResourceID + index_esES
        kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
        kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
        kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
    }
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
    kStringTableRsrcType,
    {
        kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
    }
};

resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
    k_enUS,									// Locale Id
    kEuropeanMacToWinEncodingConverter,		// Character encoding converter
    {
    // No-Translate strings go here:

    }
};




#endif // __ODFRC__

#include "N2Pwsdlclt_enUS.fr"
#include "N2Pwsdlclt_esES.fr"
#include "N2Pwsdlclt_jaJP.fr"

//  Code generated by DollyXs code generator
